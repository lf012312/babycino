class TestBugC3 {
  public static void main(String[] a) {
    System.out.println(new Test().bugging3());
  }
}

class testing3 {
    public int bugging3() {

      int x;
      int y;
      int counter;

      x = 6;
      y = 6;
      counter = 0;

      if(x<=y && y<=x) {
        counter = counter + 1;
      }
      else{
        counter = counter - 1;
      }
      return counter;
    }
}
